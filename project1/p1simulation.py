import numpy as np
import matplotlib.pyplot as plt
from mpl_toolkits.mplot3d import Axes3D
import time
dt = np.dtype(int)


def site_coordinates(sys_array, species): # species should be host, atom, or vacancy - Must have the ' ' 
    
    # We use the np.where(array == something) to find all positions of a specific atom type
    # This returns a list of 3 arrays that groups all of the x values together, and same with y and z.
        # Ex. [[x1,x2,x3],[y1,y2,y3],[z1,z2,z3]]
    # This isn't really useful for pulling out a coordinate (x,y,z)
    # We need the (x,y,z) coordinates to find the element (value) of a single site in an array
    # This function turns the list of arrays, into a list of site coordinates
    # This process will come up a bit such as trial moves, calculating energy, etc..
    
    if species == 'host':
        arg = 1  # the arg variable will tell the np.where func below what to search for
    elif species == 'atoms' or species == 'atom':
        arg = 2
    elif species == 'vacancy' or species == 'vacancies':
        arg = 0
    elif species == 'all':
        sites = np.where(sys_array >= 0)
        x,y,z = sites
        coordinates = list(zip(x,y,z))
        return coordinates, sites
    
    sites = np.where(sys_array == arg) # makes list of arrays, grouping each coordinate type together 
    x, y, z = sites # List consists of 3 elements (each array).  Each is assigned its own variable.
    coordinates = list(zip(x, y, z)) # zip combines the nth term of all 3 arrays into a single coordinate
                                    # Ex.) [(x1,y1,z1), (x2,y2,z2), (x3,y3,z3)]
    
    return coordinates, sites


def create_system (D, species, X, mixed):
    
    # Species type can be vacancy or atoms.  Default is vacancy
    # X is the concentration of whatever the non-host species is (atoms or vacancies)
    # Mixed will default to y.  If you want unmixed change to 'n'. Will always be mixed if species = vacancy

    if species == 'vacancy' or species == 'vacancies':
        site_value = 0
    elif species == 'atoms' or species == 'atom':
        site_value = 2
    else:
        print('ERROR: That species/site type is not defined in the system yet. Use "vacancy" or "atoms"')
    
    if species == 'vacancy' or species == 'vacancies':  # If second species type is vacancies, mixed always'y'
        mixed = 'y' 
    Ntot = D**3  # All lattice sites of the solid will be occupied
    N_species = int(Ntot*X) # Total number of the second species, either vacancy or 2nd atom type
    
    sys_array = np.ones((D,D,D), dtype=dt) # Creates the initial "block" of material. 3D array of ones
    shape = sys_array.shape # Stores the shape of the original 3D array into a variable
    mask = sys_array.flatten() # Makes the 3D array 1D.  Easier to replace values this way
    mask[0:N_species] = site_value # Replaces a certain amount of 1s in the 1D array with species value
                                      # Function of the concentration (X) entered as a parameter.
    if mixed == 'y':  # If species = vacancy, mixed always = yes
        np.random.shuffle(mask) # Shuffles the 1D array, so that the 1s and 0s/2s are randomly dispersed
    else:  # Doesn't mix the 1s and 2s.  We start with an unmixed system.
        pass
    
    sys_array = mask.reshape(shape) # Turns the 1D final populated array back into a 3D array
    
    return sys_array

def neighbors(atom_site, D):
    # atom_site: A single coordinate (x,y,z) that you want to find the 6 neighbors of
    # D:  The edge length of the system.  Required for periodic boundary conditions
    # Returns the coordinates of all 6 neighbors (front,back,left,right,up,down) in a list
    # The goal is to use that list to find the element (value) at each coordinate site in the atom_energy func.
    
    neighbor_list = []
    for i in range(0, 3):
        if atom_site[i] < (D-1) and atom_site[i] > 0:  # Atom site not at a boundary
            shift = [-1,1]
        elif atom_site[i] == (D-1):  # Located on the boundary opposite of origin
            shift = [-1,-(atom_site[i])] # BCs require that one of the neighbors be 0 (-atom_site[i])
        elif atom_site[i] == 0: # Located on the boundary near origin
            shift = [(D-1),1] # BCs require that one of the neighbors be at opposite edge (D-1)
        
        for num in shift:
            blank = np.zeros(3, dtype=dt)
            blank[i] = num
            new_coord = blank + atom_site
            new_coord = tuple(new_coord)
            neighbor_list.append(new_coord)
    
    return neighbor_list
    

def atom_energy(sys_array, site, D):
    
    epHost = -1.0      # Potential of host-host bonds (red atoms) array values of 1
    epAtom = -.80     # Potential of atom-atom bonds (blue atoms) array values of 2
    epHost_Atom = -.60   # Potential of Host and atom bonds
    epHost_Vac = 0.0      # Potential between a host and a neighboring vacancy 
    
    site_value = sys_array[site] # The element of our selected coordinate (0, 1, or 2)
    neighbor_list = neighbors(site, D)
    neighbor_values = []
    for i in neighbor_list:
        value = sys_array[i]
        neighbor_values.append(value)
    
    U = 0
    for n in neighbor_values:
        if 0 in [site_value, n]: # Any vacancy neighbor. 1-0, 0-0, 2-0 - All should = 0
            U = U + epHost_Vac
        elif 1 in [site_value, n] and site_value % n == 0:  # Only considers 1-1 neighbors
            if site_value == 1:
                U = U + epHost
            elif site_value == 2:
                    U = U + epHost_Atom
        elif 1 in [site_value, n] and site_value % n == 1:  # Only considers 1-2 neighbors
            U = U + epHost_Atom
        elif 2 in [site_value, n] and site_value % n == 1: # Only considers 2-1 neighbors
            U = U + epHost_Atom
        elif 2 in [site_value, n] and site_value % n == 0: # Only considers 2-2 neighbors
            U = U + epAtom

    return U

def trial_move(sys_array, species, T, D):

    # Going to use the atom_energy function to find the change in PE of the moved sites only
    # We won't need to calculate the energy of the entire system every time.
    # Only going to calculate the deltaE of the single move, and keep track of that.
    # The final energy can be found by adding the sum of our deltaEs to the initial energy
    
    # Define all of the positions of each type in the array:
    host_coords, host_sites = site_coordinates(sys_array,'host')
    species_coords, species_sites = site_coordinates(sys_array,species)
       
    # Random select site to be moved, and location to move it to. Basically swaps sites
    # Right now, only considering movements that swaps 2 unlike sites (host to vacancy, host to atom)
    np.random.shuffle(host_coords)
    np.random.shuffle(species_coords) # Randomize the order of the list of coordinate positions

    site1 = host_coords[0]  # Pick the first item in the list. Retruns a COORDINATE (x,y,z)
    site2 = species_coords[0]
    site1_value = sys_array[site1] # Returns the element (value) from the host coordinate. Always 1
    site2_value = sys_array[site2] # The value from the species list - 0 or a 2
    
    # Inital PE calculations of both sites (before anything is changed):  PE1 = Initial PE
    PE1_site1 = atom_energy(sys_array, site1, D)
    PE1_site2 = atom_energy(sys_array, site2, D)
    PE1_tot = PE1_site1 + PE1_site2 # Total PE of both sites before they are switched with eachother.

    # Make the swap of the values at the 2 sites:
    sys_array[site1] = site2_value
    sys_array[site2] = site1_value
        
    # Calculate the PE of each site after the swap:
    PE2_site1 = atom_energy(sys_array, site1, D)
    PE2_site2 = atom_energy(sys_array, site2, D)
    PE2_tot = PE2_site1 + PE2_site2

    
    deltaPE = PE2_tot - PE1_tot # Change in PE resulting from the move

    
    if deltaPE <= 0 or np.random.random() < np.exp(-deltaPE/T): # accept the move, keep changed site values.
        host_coords, host_sites = site_coordinates(sys_array,'host')
        species_coords, species_sites = site_coordinates(sys_array,species)
        move_status = 'accepted'
        return sys_array, deltaPE, host_sites, species_sites, move_status
    
    else: # If the move is not accepted
        sys_array[site1] = site1_value  # Change the 2 site values back to their initial values
        sys_array[site2] = site2_value
        deltaPE = deltaPE * 0 # need to return 2 variables to match the return statement above.
                              # Changed deltaPE to zero since the move is rejected, and there is no change in E
        host_coords, host_sites = site_coordinates(sys_array,'host')
        species_coords, species_sites = site_coordinates(sys_array,species)
        move_status = 'rejected'
        return sys_array, deltaPE, host_sites, species_sites, move_status


def run_simulation(steps, species, D, X, mixed, T, sys_array = 'new'):
    initial_t = time.time()
    if sys_array == 'new':
        sys_array = create_system(D, species, X, mixed) # Initial system array
    initial_sys_array = np.copy(sys_array)
    print('Running Simulaton...')
    
    U = 0 # Will show the change in energy.  The initial value is arbitrary, going to use zero.
    U_list = []
    moves = []
    for step in range(steps):
        sys_array, deltaPE, host_sites, species_sites, move_status = trial_move(sys_array, species, T, D) # make a move
        U = U + deltaPE # Add the resulting change in energy to U
        U_list.append(U)
        moves.append(move_status)
        if step == steps - 1:
            final_sys_array = np.copy(sys_array)
    accepted_moves = ['accepted' for i in moves if i == 'accepted']
    final_t = time.time()
    print('Simulation Complete:')
    print("Computation time = {:0.2f} s".format(final_t - initial_t))
    print('Ratio of accepted moves = {}'.format(len(accepted_moves)/len(moves)))

    return U_list, initial_sys_array, final_sys_array, moves


